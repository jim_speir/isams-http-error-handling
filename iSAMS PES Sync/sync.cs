﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data.SqlClient;
using iSAMS_PES_Sync.PESmisAPI;
using System.IO;
using System.Xml;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace iSAMS_PES_Sync
{
    class Sync
    {
        private PESmisClient PESClient;
        private string sysname;
        private string apikey;
        private string iSAMSapiKey;
        private string iSAMSwebaddress;
        private XElement schoolData;
        private bool preferredNames = false;

        internal PESmisClient createClient()
        {
            PESmisClient client = null;
            try
            {
                client = new PESmisClient();
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldn't create PES API client: " + e.Message);
            }
            client.ClientCredentials.UserName.UserName = sysname;
            client.ClientCredentials.UserName.Password = apikey;
            return client;
        }

        public Sync(string sysname)
        {
            this.sysname = sysname;


            using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.pesConnString))
            {
                conn.Open();
                using (SqlCommand com = new SqlCommand())
                {
                    com.Connection = conn;
                    com.Parameters.Clear();
                    com.CommandText = "SELECT APIKey, iSAMSwebaddress, iSAMSapiKey, preferredNames FROM config INNER JOIN iSAMSsync ON config.sysname = iSAMSsync.sysname WHERE config.sysname = @sysname";
                    com.Parameters.AddWithValue("@sysname", sysname);
                    using (SqlDataReader dr = com.ExecuteReader())
                        if (dr.Read())
                        {
                            try { this.apikey = dr.GetString(0); }
                            catch (Exception e) { Exit(); }
                            try { this.iSAMSwebaddress = dr.GetString(1); }
                            catch (Exception e) { Exit(); }
                            try { this.iSAMSapiKey = dr.GetString(2); }
                            catch (Exception e) { Exit(); }
                            try { this.preferredNames = dr.GetBoolean(3); }
                            catch (Exception e) { }
                        }
                }
            }

        }

        public Sync(string sysname, string apiKey, string iSAMSwebaddress, string iSAMSapiKey, bool preferredNames)
        {
            this.sysname = sysname;
            this.apikey = apiKey;
            this.iSAMSwebaddress = iSAMSwebaddress;
            this.iSAMSapiKey = iSAMSapiKey;
            this.preferredNames = preferredNames;
        }

        public async Task processSystem()
        {
            PESClient = createClient();
            try
            {
                Console.WriteLine("Connecting to API for sysname \"{0}\" and licence key \"{1}\"", PESClient.ClientCredentials.UserName.UserName, PESClient.ClientCredentials.UserName.Password);
                PESClient.Open();
            }
            catch (System.ServiceModel.Security.MessageSecurityException ex)
            {
                Console.WriteLine(ex.Message);
                Program.logEntry(ex.Message);
                // Authentication exception, let the user know what happened.
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                    Program.logEntry(ex.InnerException.Message);
                }
                // propagate exception, or exit, whatever
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Program.logEntry(ex.Message);
                // Authentication exception, let the user know what happened.
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                    Program.logEntry(ex.InnerException.Message);
                }
                // propagate exception, or exit, whatever
            }

            Console.WriteLine("Connected, running sync.");

            if (await getData())
            {
                Console.WriteLine("Got data from iSAMS successfully");

                doStudents();
                doContacts();
                doTeachers();
                doClasses();
                doGroups();
            }
            else
            {
                Console.WriteLine("No data returned");
                Console.ReadLine();
            }

            PESClient.finished();
            Console.WriteLine("Finished " + sysname);
        }

        private async Task<bool> getData()
        {
            /*
            client.Encoding = Encoding.UTF8;
            client.QueryString.Add("apiKey", iSAMSapiKey);
            string downloadData;
            if (!iSAMSwebaddress.StartsWith("http"))
                iSAMSwebaddress = "https://" + iSAMSwebaddress;

            //create filters XML
            */
            MemoryStream filtersStream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(filtersStream))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Filters");
                writer.WriteStartElement("StudentManager");

                writer.WriteStartElement("Contacts");
                writer.WriteAttributeString("SystemStatusesToInclude", "0,1");
                
                writer.WriteEndElement();
            }

            HttpContent streamedContent = new StreamContent(filtersStream);
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/xml"));
            client.Timeout = new TimeSpan(0, 10, 0);
            Console.WriteLine("https://" + iSAMSwebaddress + "/api/batch/1.0/xml.ashx?apiKey=" + iSAMSapiKey);
            HttpResponseMessage response;
            try
            {
                response = await client.PostAsync("https://" + iSAMSwebaddress + "/api/batch/1.0/xml.ashx?apiKey=" + iSAMSapiKey, streamedContent);
            }
            catch(Exception e)
            {
                throw e;
            }

            if(response.IsSuccessStatusCode)
            {
                try
                {
                    var downloadData = response.Content.ReadAsStringAsync().Result;
                    //the iSAMS API can be buggy and send an HTML error page after a successful XML response. Strip out anything after the closing </iSAMS> tag.
                    int index = downloadData.IndexOf("</iSAMS>", StringComparison.InvariantCultureIgnoreCase);
                    if (index > 0)
                        downloadData = downloadData.Substring(0, index + 8); //return a string from the start till the index of the closing tag, plus the 8 char "</iSAMS>"

                    try
                    {
                        schoolData = XElement.Parse(downloadData);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error parsing XML file: " + e.Message);
                        return false;
                    }
                    bool HRManagerExists = schoolData.Element("HRManager") != null; // Check if we have actually received data
                    if (!HRManagerExists)
                        Console.WriteLine("HRManager doesn't exist. First few lines from iSAMS:\n" + schoolData.ToString().TruncateLongString(200));

                    return HRManagerExists;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while stripping invalid characters at end of XML response: " + e.Message);
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Failed to get data from iSAMS");
                Console.WriteLine("Connected to: " + response.RequestMessage.RequestUri);
                Console.WriteLine("Result code: " + response.StatusCode);
                return false;
            }
        }


        private void doStudents()
        {
            List<Student> studentsToSend = new List<Student>();

            var queryStudents = from e in schoolData.Descendants("Pupil").Where(p => p.Element("NCYear") != null)
                                join y in schoolData.Descendants("SchoolManager").Elements("Years").Descendants("Year") on (int)e.Element("NCYear") equals (int)(y.Attribute("id") ?? y.Attribute("Id")) into yy
                                from y in yy.DefaultIfEmpty()
                                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null
                             select new Student
                             {
                                 forename = (preferredNames && e.Element("Preferredname") != null ? (string)e.Element("Preferredname") : (string)e.Element("Forename")),
                                 surname = (string)e.Element("Surname"),
                                 dob = ((string)e.Element("DOB")).TryGetDate(),
                                 regclass = e.Element("Form") == null ? "" : ((string)e.Element("Form")),
                                 year = y != null ? (string)y.Element("Name") : (string)e.Element("NCYear"),
                                 externalID = (string)e.Element("SchoolId")
                             };

            studentsToSend = queryStudents.ToList();

            foreach (var s in studentsToSend)
                if (s.forename == null)
                    Console.WriteLine(s.externalID);

            if (studentsToSend.Count == 0)
            {
                Console.WriteLine("No students returned from iSAMS, exiting");
                Exit();
                return;
            }

            try
            {
                Console.WriteLine("\nSending Students to API (" + studentsToSend.Count + " entries)");
                Console.WriteLine(PESClient.SetStudents(studentsToSend));
            }
            catch (Exception e) //most likely System.ServiceModel.Security.MessageSecurityException
            {
                PESClient.Abort();
                try
                {
                    PESClient = createClient();
                    PESClient.Open();
                    Console.WriteLine("\nSending Students to API (" + studentsToSend.Count + " entries)");
                    Console.WriteLine(PESClient.SetStudents(studentsToSend));
                }
                catch (Exception ex)
                {
                    // Authentication exception, let the user know what happened.
                    if (ex.InnerException != null)
                    {
                        Console.WriteLine("Error sending Students: " + ex.InnerException.Message);
                    }
                    else
                    {
                        Console.WriteLine("Error sending Students: " + ex.Message);
                    }
                    Exit();
                }
            }
        }

        private void doContacts()
        {
            List<Contact> contactsToSend = new List<Contact>();

            List<string> allowedRelationships = new List<string>
            {
                "Mother",
                "Father",
                "Guardian",
                "Step"
            };

            var queryParentStudents =
                (from p in schoolData.Descendants("Contact")
                    .Where(p =>
                        (p.Element("RelationshipRaw") != null
                        && allowedRelationships.Any(a =>
                            ((string)p.Element("RelationshipRaw")).StartsWith(a, StringComparison.OrdinalIgnoreCase)))
                        || (string)p.Element("StudentHome") == "1")
                 from s in p.Descendants("Pupil")
                 select new
                 {
                     Parent = p,
                     Student = s
                 })
                .ToList();

            var test = queryParentStudents.Where(ps => (string)ps.Parent.Attribute("Id") == "1112");

            var dictIdRelationshipCounts = new Dictionary<string, int>();
            var queryContacts = new List<Contact>();

            foreach (var ps in queryParentStudents)
            {
                var key = createIdRelationshipKey(
                    (string)ps.Parent.Attribute("Id"),
                    (string)ps.Student.Element("SchoolId"),
                    (string)ps.Parent.Element("Relationship"));

                var initialExternalId = (string)ps.Parent.Attribute("Id") + (string)ps.Parent.Element("Relationship");

                var externalId = initialExternalId;

                var isFirstPersonContact =
                    ps.Parent.Attribute("IsFirstPersonContact") != null &&
                    (bool)ps.Parent.Attribute("IsFirstPersonContact") == true;
                
                if (dictIdRelationshipCounts.ContainsKey(key))
                {
                    var count = dictIdRelationshipCounts[key]++;

                    if (!isFirstPersonContact)
                    {
                        if(count > 1)
                        {
                            externalId = $"{initialExternalId}-{count}";
                        }
                    }

                }
                else
                {
                    dictIdRelationshipCounts.Add(key, 1);
                }

                queryContacts.Add(new Contact
                {
                    title = (ps.Parent.Element("Title") == null
                        ? null
                        : ((string)ps.Parent.Element("Title")).Replace(".", "")),
                    forename = (string)ps.Parent.Element("Forename"),
                    surname = (string)ps.Parent.Element("Surname"),
                    email = (string)ps.Parent.Element("EmailAddress"),
                    phone = (string)ps.Parent.Element("Telephone"),
                    studentExternalID = (string)ps.Student.Element("SchoolId"),
                    externalID = externalId,
                    relationship = (string)ps.Parent.Element("RelationshipRaw")
                });

            }
            
            contactsToSend = queryContacts
                .Where(c => (c.title != null || c.forename != null) && c.surname != null)
                .Distinct(new ContactComparer())
                .ToList();

            try

            {
                Console.WriteLine("\nSending Contacts to API (" + contactsToSend.Count + " entries)");
                Console.WriteLine(PESClient.SetContacts(contactsToSend));
            }
            catch (Exception e) //most likely System.ServiceModel.Security.MessageSecurityException
            {
                PESClient.Abort();
                try
                {
                    PESClient = createClient();
                    PESClient.Open();
                    Console.WriteLine("\nSending Students to API (" + contactsToSend.Count + " entries)");
                    Console.WriteLine(PESClient.SetContacts(contactsToSend));
                }
                catch (Exception ex)
                {
                    // Authentication exception, let the user know what happened.
                    if (ex.InnerException != null)
                    {
                        Console.WriteLine("Error sending Contacts: " + ex.InnerException.Message);
                    }
                    else
                    {
                        Console.WriteLine("Error sending Contacts: " + ex.Message);
                    }
                    Exit();
                }
            }
        }

        private string createIdRelationshipKey(string id, string studentId, string relationship) => $"{id}:{studentId}:{relationship}";

        private void doTeachers()
        {
            List<Teacher> teachersToSend = new List<Teacher>();

            var queryTeachers = from e in schoolData.Descendants("HRManager").Descendants("StaffMember")
                                let ts = from t in schoolData.Descendants("Sets").Descendants("Set").Elements("Teachers").Elements("Teacher")
                                         select (int)t.Attribute("StaffId")
                                let fs = from f in schoolData.Descendants("SchoolManager").Elements("Forms").Descendants("Form")
                                         where f.Attribute("TutorId") != null
                                         select new {
                                             TutorID = (int)f.Attribute("TutorId"),
                                             AssistantTutorID =  f.Attribute("AssistantTutorId") != null ? (int)f.Attribute("AssistantTutorId") : -1,
                                             SecondAssistantTutorID = f.Attribute("SecondAssistantTutorId") != null ? (int)f.Attribute("SecondAssistantTutorId") : -1
                                        }
                                let hs = from f in schoolData.Descendants("House")
                                         where f.Attribute("HouseMasterId") != null
                                         select (int)f.Attribute("HouseMasterId")
                                let tf = from f in schoolData.Descendants("TeachingForm")
                                         join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (string)f.Element("Teacher") equals (string)sm.Element("UserCode")
                                         where f.Element("Teacher") != null
                                         select (int)sm.Attribute("Id")
                                let yg = from y in schoolData.Descendants("Year")
                                         where y.Attribute("TutorId") != null
                                         select new
                                         {
                                            TutorID = (int)y.Attribute("TutorId"),
                                            AssistantTutorID = y.Attribute("AssistantTutorId") != null ? (int)y.Attribute("AssistantTutorId") : -1
                                         }
                                where 
                                    ts.Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) || 

                                    fs.Select(f => f.TutorID)
                                      .Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) ||

                                    fs.Where(f => f.AssistantTutorID != -1)
                                      .Select(f => f.AssistantTutorID)
                                      .Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) ||

                                    fs.Where(f => f.SecondAssistantTutorID != -1)
                                      .Select(f => f.SecondAssistantTutorID)
                                      .Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) || 

                                    hs.Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) || 

                                    tf.Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) ||

                                    yg.Select(f => f.TutorID)
                                      .Contains((int)(e.Attribute("id") ?? e.Attribute("Id"))) ||

                                    yg.Where(f => f.AssistantTutorID != -1)
                                      .Select(f => f.AssistantTutorID)
                                      .Contains((int)(e.Attribute("id") ?? e.Attribute("Id")))
                                select new Teacher
                                {
                                    forename = (preferredNames ? (e.Element("PreferredName") == null ? "" : (string)e.Element("PreferredName")) : (e.Element("Forename") == null ? "" : (string)e.Element("Forename"))),
                                    surname = (e.Element("Surname") == null ? "" : (string)e.Element("Surname")),
                                    title = (e.Element("Title") == null ? "" : (string)e.Element("Title")),
                                    email = (e.Element("SchoolEmailAddress") == null ? "" : (string)e.Element("SchoolEmailAddress")),
                                    room = String.Empty,
                                    externalID = (string)e.Element("UserCode")
                                };

            teachersToSend = queryTeachers.ToList();

            if (teachersToSend.Count == 0)
            {
                Console.WriteLine("No teachers returned from iSAMS, exiting");
                Exit();
                return;
            }

            try
            {
                Console.WriteLine("\nSending teachers to API (" + teachersToSend.Count + " entries)");
                Console.WriteLine(PESClient.SetTeachers(teachersToSend));
            }
            catch (Exception e) //most likely System.ServiceModel.Security.MessageSecurityException
            {
                PESClient.Abort();
                try
                {
                    PESClient = createClient();
                    PESClient.Open();
                    Console.WriteLine("\nSending teachers to API (" + teachersToSend.Count + " entries)");
                    Console.WriteLine(PESClient.SetTeachers(teachersToSend));
                }
                catch (Exception ex)
                {
                    // Authentication exception, let the user know what happened.
                    if (ex.InnerException != null)
                    {
                        Console.WriteLine("Error sending teachers: " + ex.InnerException.Message);
                    }
                    else
                    {
                        Console.WriteLine("Error sending teachers: " + ex.Message);
                    }
                    Exit();
                }
            }
        }


        private void doClasses()
        {
            List<Class> classesToSend = new List<Class>();

            var queryClasses = 
                from s in schoolData.Descendants("SetList")
                join f in schoolData.Descendants("Sets").Descendants("Set") on (int)s.Attribute("SetId") equals (int)(f.Attribute("id") ?? f.Attribute("Id"))
                join t in schoolData.Descendants("Sets").Descendants("Set").Elements("Teachers").Elements("Teacher") on (int)s.Attribute("SetId") equals (int)(t.Attribute("id") ?? t.Attribute("Id"))
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)t.Attribute("StaffId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))
                join sb in schoolData.Descendants("TeachingManager").Descendants("Departments").Descendants("Subject") on f.Attribute("SubjectId") == null ? 
                    -1 : 
                    (int)f.Attribute("SubjectId") equals (int)(sb.Attribute("id") ?? sb.Attribute("Id"))
                join p in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null && p.Parent.Name == "CurrentPupils") on (int)s.Attribute("PupilId") equals (int)(p.Attribute("id") ?? p.Attribute("Id"))
                                
                select new Class
                    {
                        studentExternalID = (string)p.Element("SchoolId"),
                        teacherExternalID = (string)sm.Element("UserCode"),
                        className = (string)f.Element("Name"),
                        subject = (string)sb.Element("Name"),
                        externalID = (int)s.Attribute("SetId"),
                        groupType = GroupType.Class
                    };

            classesToSend = queryClasses.ToList();



            var queryForms = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                join f in schoolData.Descendants("SchoolManager").Elements("Forms").Descendants("Form").Where(f => f.Attribute("TutorId") != null) on (string)e.Element("Form") equals (string)f.Element("Form")
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)f.Attribute("TutorId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))   
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)sm.Element("UserCode"),
                    className = "Form " + (string)f.Element("Form"),
                    subject = (string)f.Element("Form"),
                    mainTeacher = true,
                    externalID = (f.Attribute("id") ?? f.Attribute("Id")).GetHashCode(),
                    groupType = GroupType.Registration
                };

            classesToSend.AddRange(queryForms.ToList());


            var queryFormsAssistant = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                join f in schoolData.Descendants("SchoolManager").Elements("Forms").Descendants("Form").Where(f => f.Attribute("AssistantTutorId") != null) on (string)e.Element("Form") equals (string)f.Element("Form")
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)f.Attribute("AssistantTutorId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)sm.Element("UserCode"),
                    className = "Form " + (string)f.Element("Form"),
                    subject = (string)f.Element("Form"),
                    externalID = (f.Attribute("id") ?? f.Attribute("Id")).GetHashCode(),
                    groupType = GroupType.Registration
                };

            classesToSend.AddRange(queryFormsAssistant.ToList());


            var queryFormsSecondAssistant = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                join f in schoolData.Descendants("SchoolManager").Elements("Forms").Descendants("Form").Where(f => f.Attribute("SecondAssistantTutorId") != null) on (string)e.Element("Form") equals (string)f.Element("Form")
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)f.Attribute("SecondAssistantTutorId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)sm.Element("UserCode"),
                    className = "Form " + (string)f.Element("Form"),
                    subject = (string)f.Element("Form"),
                    externalID = (f.Attribute("id") ?? f.Attribute("Id")).GetHashCode(),
                    groupType = GroupType.Registration
                };

            classesToSend.AddRange(queryFormsSecondAssistant.ToList());

            var queryYearHeads = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("NCYear") != null)
                join y in schoolData.Descendants("SchoolManager").Elements("Years").Descendants("Year").Where(y => y.Attribute("TutorId") != null) on (int)e.Element("NCYear") equals (int)(y.Attribute("id") ?? y.Attribute("Id"))
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)y.Attribute("TutorId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))
                where y.Parent.Name == "Years" && y.Parent.Parent.Name == "SchoolManager" && e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null && y.Attribute("TutorId") != null
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)sm.Element("UserCode"),
                    className = (string)y.Element("Code") + " Year Head",
                    subject = (string)y.Element("Code") + " Year Head",
                    externalID = ("Year" + (y.Attribute("id") ?? y.Attribute("Id"))).GetHashCode(),
                    groupType = GroupType.Year
                };


            classesToSend.AddRange(queryYearHeads.ToList());

            var queryHouseHeads = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null && e.Element("AcademicHouse") != null
                join h in schoolData.Descendants("AcademicHouses").Descendants("House").Where(h => h.Attribute("HouseMasterId") != null) on (string)e.Element("AcademicHouse") equals (string)h.Element("Name")
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)h.Attribute("HouseMasterId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)sm.Element("UserCode"),
                    className = (string)e.Element("AcademicHouse"),
                    subject = (string)e.Element("AcademicHouse"),
                    externalID = ("House" + (h.Attribute("id") ?? h.Attribute("Id"))).GetHashCode(),
                    groupType = GroupType.House
                };

            classesToSend.AddRange(queryHouseHeads.ToList());

            var queryBoardingHouseHeads = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null && e.Element("BoardingHouse") != null
                join h in schoolData.Descendants("BoardingHouses").Descendants("House").Where(h => h.Attribute("HouseMasterId") != null) on (string)e.Element("BoardingHouse") equals (string)h.Element("Name")
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (int)h.Attribute("HouseMasterId") equals (int)(sm.Attribute("id") ?? sm.Attribute("Id"))
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)sm.Element("UserCode"),
                    className = (string)e.Element("BoardingHouse"),
                    subject = (string)e.Element("BoardingHouse"),
                    externalID = ("BoardingHouse" + (h.Attribute("id") ?? h.Attribute("Id"))).GetHashCode(),
                    groupType = GroupType.House
                };

            classesToSend.AddRange(queryBoardingHouseHeads.ToList());

            var queryFormBasedSubjects = 
                from h in schoolData.Descendants("TeachingForm").Where(t => t.Element("FormCode") != null)
                join e in schoolData.Descendants("Pupil").Where(p => p.Element("Form") != null) on (string)h.Element("FormCode") equals (string)e.Element("Form")
                join sb in schoolData.Descendants("TeachingManager").Descendants("Departments").Descendants("Subject") on (int)h.Element("SubjectId") equals (int)(sb.Attribute("id") ?? sb.Attribute("Id"))
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)h.Element("Teacher"),
                    className = (string)h.Element("FormCode"),
                    subject = (string)sb.Element("Name"),
                    externalID = ("TeachingForm" + (h.Attribute("id") ?? h.Attribute("Id"))).GetHashCode(),
                    groupType = GroupType.Class
                };

            classesToSend.AddRange(queryFormBasedSubjects.ToList());

            var queryPersonalTutor = 
                from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null && e.Element("Tutor") != null
                join sm in schoolData.Descendants("HRManager").Descendants("StaffMember") on (string)e.Element("Tutor") equals (string)sm.Element("UserCode")
                select new Class
                {
                    studentExternalID = (string)e.Element("SchoolId"),
                    teacherExternalID = (string)e.Element("Tutor"),
                    className = "Personal Tutor " + (string)sm.Element("Initials"),
                    subject = "Personal Tutor",
                    externalID = ("Personal Tutor " + (string)e.Element("Tutor")).GetHashCode(),
                    groupType = GroupType.Registration
                };

            classesToSend.AddRange(queryPersonalTutor.ToList());

            if (classesToSend.Count == 0)
            {
                Console.WriteLine("No classes returned from iSAMS, exiting");
                Exit();
                return;
            }

            try
            {
                Console.WriteLine("\nSending classes to API (" + classesToSend.Count + " entries)");
                Console.WriteLine(PESClient.SetClasses(classesToSend));
            }
            catch (Exception e) //most likely System.ServiceModel.Security.MessageSecurityException
            {
               PESClient.Abort();
                try
                {
                    PESClient = createClient();
                    PESClient.Open();
                    Console.WriteLine("\nSending classes to API (" + classesToSend.Count + " entries)");
                    Console.WriteLine(PESClient.SetClasses(classesToSend));
                }
                catch (Exception ex)
                {
                    // Authentication exception, let the user know what happened.
                    if (ex.InnerException != null)
                    {
                        Console.WriteLine("Error sending classes: " + ex.InnerException.Message);
                    }
                    else
                    {
                        Console.WriteLine("Error sending classes: " + ex.Message);
                    }
                    Exit();
                }
            }
        }

        private void Exit()
        {
            Environment.Exit(1);
        }

        private void doGroups()
        {
            List<Group> groupsToSend = new List<Group>();

            var queryHouseGroups = from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                                   where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null && e.Element("AcademicHouse") != null
                                   && schoolData.Descendants("AcademicHouses") == null //only send houses as groups if we don't have academic house masters listed
                                   select new Group
                                   {
                                       studentExternalID = (string)e.Element("SchoolId"),
                                       name = (string)e.Element("AcademicHouse"),
                                       externalID = ((string)e.Element("AcademicHouse") + (string)e.Element("SchoolId")).GetHashCode(),
                                       groupType = GroupType.House
                                   };

            groupsToSend = queryHouseGroups.ToList();

            var queryBoardingGroups = from e in schoolData.Descendants("Pupil").Where(p => p.Element("SchoolId") != null)
                                   where e.Element("Forename") != null && e.Element("Surname") != null && e.Element("NCYear") != null && e.Element("SchoolId") != null && e.Element("BoardingHouse") != null
                                   && schoolData.Descendants("BoardingHouse") == null //only send houses as groups if we don't have boarding house masters listed
                                   select new Group
                                   {
                                       studentExternalID = (string)e.Element("SchoolId"),
                                       name = (string)e.Element("BoardingHouse"),
                                       externalID = ((string)e.Element("BoardingHouse") + (string)e.Element("SchoolId")).GetHashCode(),
                                       groupType = GroupType.House
                                   };

            groupsToSend.AddRange(queryHouseGroups.ToList());

            var queryDivisions = from e in schoolData.Descendants("Pupil").Where(p => p.Element("NCYear") != null)
                                 join y in schoolData.Descendants("SchoolManager").Elements("Years").Descendants("Year") on (int)e.Element("NCYear") equals (int)(y.Attribute("id") ?? y.Attribute("Id"))
                                 join sd in schoolData.Descendants("Year").Where(sdd => sdd.Parent.Parent.Name == "SchoolDivision") on (int)(y.Attribute("id") ?? y.Attribute("Id")) equals (int)sd.Attribute("YearId")
                                   select new Group
                                   {
                                       studentExternalID = (string)e.Element("SchoolId"),
                                       name = (string)sd.Parent.Parent.Element("Name"),
                                       externalID = sd.Parent.Parent.Element("Code").GetHashCode(),
                                       groupType = GroupType.Band
                                   };

            groupsToSend.AddRange(queryDivisions.ToList());

            var queryCustomGroups = from e in schoolData.Descendants("CustomPupilGroupMembershipItems").Elements("CustomPupilGroupMembershipItem").Where(c => c.Element("CustomGroupId") != null)
                                    join y in schoolData.Descendants("CustomPupilGroups").Elements("CustomPupilGroup") on (int)e.Element("CustomGroupId") equals (int)(y.Attribute("id") ?? y.Attribute("Id"))
                                    where ((int?)e.Element("Deleted") ?? 0) == 0 && ((int?)y.Element("Deleted") ?? 0) == 0
                                    select new Group
                                    {
                                        studentExternalID = (string)e.Element("SchoolId"),
                                        name = (string)y.Element("Name"),
                                        externalID = (int)(y.Attribute("id") ?? y.Attribute("Id")),
                                        groupType = GroupType.Custom
                                    };

            groupsToSend.AddRange(queryCustomGroups.ToList());


            if (groupsToSend.Count > 0)
            {
                try
                {
                    Console.WriteLine("\nSending groups to API (" + groupsToSend.Count + " entries)");
                    Console.WriteLine(PESClient.SetGroups(groupsToSend));
                }
                catch (Exception e) //most likely System.ServiceModel.Security.MessageSecurityException
                {
                    PESClient.Abort();
                    try
                    {
                        PESClient = createClient();
                        PESClient.Open();
                        Console.WriteLine("\nSending groups to API (" + groupsToSend.Count + " entries)");
                        Console.WriteLine(PESClient.SetGroups(groupsToSend));
                    }
                    catch (Exception ex)
                    {
                        // Authentication exception, let the user know what happened.
                        if (ex.InnerException != null)
                        {
                            Console.WriteLine("Error sending groups: " + ex.InnerException.Message);
                        }
                        else
                        {
                            Console.WriteLine("Error sending groups: " + ex.Message);
                        }
                    }
                }
            }
        }
    }
}
