﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace iSAMS_PES_Sync
{
    class Program
    {


        static void Main(string[] args)
        {
#if !DEBUG
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
#endif
            Console.WriteLine("Starting iSAMS PES Sync...");
            List<string> systemsToSync = new List<string>();
            if (args.Length == 1)
                systemsToSync.Add(args[0].Trim());
            else if (args.Length == 5)
            {
                Sync tempSync = new Sync(args[0].Trim(), args[1].Trim(), args[2].Trim(), args[3].Trim(), Convert.ToBoolean(args[4].Trim()));
                Task.WaitAll(tempSync.processSystem());
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.pesConnString))
                {
                    conn.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.Connection = conn;
                        com.Parameters.Clear();
                        com.CommandText = "SELECT sysname FROM iSAMSsync WHERE ISNULL(runSync,1) = 'True' ORDER BY ID DESC";
                        using (SqlDataReader dr = com.ExecuteReader())
                            while (dr.Read())
                                systemsToSync.Add(dr.GetString(0));
                    }
                }
            }

            foreach (var sysname in systemsToSync)
            {
                Sync tempSync = new Sync(sysname);
                Task.WaitAll(tempSync.processSystem());
            }

            Console.WriteLine("Finished processing all systems");     

        }

        //this is a new method in the class
        /// <summary>
        /// Occurs when you have an unhandled exception
        /// </summary>
        public static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //get the exception and do something with it, optional
            Exception exception = (Exception)e.ExceptionObject;
            //output to console
            Console.WriteLine("There was an error...");
            Console.Write(exception.Message + "\n" + exception.StackTrace);
            //exit
            System.Environment.Exit(1);
        }



        /// <summary>
        /// Trace function that simply writes to the designated log.
        /// </summary>
        /// <param name="entry"></param>
        internal static void logEntry(string entry, params object[] args)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Path.Combine(Application.StartupPath, "iSAMSPESsyncLog.txt"), true)) // Append mode
            {
                sw.WriteLine(System.DateTime.Now.ToString() + ":   " + String.Format(entry, args));
                sw.Flush();
                sw.Close();
            }
        }
    }
}
