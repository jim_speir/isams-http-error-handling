﻿using iSAMS_PES_Sync.PESmisAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iSAMS_PES_Sync
{

    public class ContactComparer : IEqualityComparer<Contact>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(Contact x, Contact y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.studentExternalID == y.studentExternalID && x.externalID == y.externalID;
        }

        public int GetHashCode(Contact x)
        {
            return x.studentExternalID.GetHashCode() * 10000 + x.externalID.GetHashCode();
        }
    }
}
