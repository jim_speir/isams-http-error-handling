﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace iSAMS_PES_Sync
{
    public static class LINQHelper
    {
        public static DateTime? TryGetDate(this string dateString, string format = null)
        {
            DateTime dt;
            bool success = format == null ? DateTime.TryParse(dateString, out dt) : DateTime.TryParseExact(dateString, format, null, DateTimeStyles.None, out dt);
            return success ? dt : (DateTime?)null;
        }

        public static string TruncateLongString(this string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }
    }
}
